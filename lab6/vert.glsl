#version 330

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

in vec3 position;
in vec3 color;
in vec3 normal;

out vec3 fpos;
out vec3 fcolor;
out vec3 fnormal;

void main() {
  gl_Position = projection * view * model * vec4(position, 1);
  fcolor = color;
  fpos = (view*model*vec4(position, 1)).xyz;
  fnormal = normalize(transpose(inverse(view * model)) * vec4(normal, 0)).xyz;
}

// P = view*model*position
// N = normalize(transpose(inverse(view*model*normal)))
// L = normalize(lightPos - P)
// diff = max(dot(N, L), 0)
// V = normalize(-P)
// R = 2*dot(L, N) * N - L
// spec = max(0, pow(dot(R, V), alpha))
