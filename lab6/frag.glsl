#version 330

in vec3 fpos;
in vec3 fcolor;
in vec3 fnormal;

out vec4 color_out;

uniform vec3 lightPos;
uniform float alpha;
uniform mat4 view;
uniform mat4 model;

void main() {
	vec3 P = fpos;
	vec3 N = normalize(fnormal);
	vec3 light = (view * model * vec4(lightPos, 1)).xyz;
	vec3 L = normalize(light - P);
	float diffuse = 1.1*max(dot(N, L), 0);
	vec3 V = normalize(-P);
	vec3 R = 2*dot(L, N)*N - L;
	float specular = 0.9*max(0, pow(dot(R, V), alpha));
	float amb = 0.1;
	color_out = vec4(diffuse*fcolor + specular + amb*fcolor, 1);
}

// P = view*model*position
// N = normalize(transpose(inverse(view*model*normal)))
// L = normalize(lightPos - P)
// diff = max(dot(N, L), 0)
// V = normalize(-P)
// R = 2*dot(L, N) * N - L
// spec = max(0, pow(dot(R, V), alpha))
