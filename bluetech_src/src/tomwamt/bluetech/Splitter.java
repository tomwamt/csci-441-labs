package tomwamt.bluetech;

import java.util.Arrays;

public class Splitter extends Block{
	private Side inputSide;
	private Side output0Side;
	private Side output1Side;
	private int output0Width;
	private int output1Width;
	
	public Splitter(int output0Width, int output1Width){
		this.output0Width = output0Width;
		this.output1Width = output1Width;
		setValues(new boolean[output0Width+output1Width]);
		inputSide = Side.DOWN;
		output0Side = Side.UP;
		output1Side = Side.RIGHT;
	}
	
	@Override
	public void update(FunctionBlock parent) {
		setUpdated(true);
		boolean[] inputValues;
		Block inputBlock = parent.getBlock(getLocation().getAdjacent(inputSide));
		
		if(inputBlock != null && inputBlock.getOutputWireWidth(inputSide.opposite()) == (output0Width+output1Width)){
			if(!inputBlock.hasUpdated())
				inputBlock.update(parent);
			inputValues = inputBlock.getOutput(inputSide.opposite());
		}else{
			inputValues = new boolean[output0Width+output1Width];
		}
		
		setValues(inputValues);
	}

	@Override
	public void tick() {
		setUpdated(false);
	}

	@Override
	public int getInputWireWidth(Side side) {
		if(side.equals(inputSide))
			return output0Width+output1Width;
		else
			return 0;
	}

	@Override
	public int getOutputWireWidth(Side side) {
		if(side.equals(output0Side))
			return output0Width;
		else if(side.equals(output1Side))
			return output1Width;
		else
			return 0;
	}

	@Override
	public boolean[] getOutput(Side side) {
		if(side.equals(output0Side))
			return Arrays.copyOfRange(getValues(), 0, output0Width);
		else if(side.equals(output1Side))
			return Arrays.copyOfRange(getValues(), output0Width, output0Width+output1Width);
		else
		return null;
	}

	public void setLowOutputWidth(int width){
		output0Width = width;
		setValues(new boolean[output0Width+output1Width]);
	}
	
	public void setHighOutputWidth(int width){
		output1Width = width;
		setValues(new boolean[output0Width+output1Width]);
	}
	
	public void setInputSide(Side inputSide){
		this.inputSide = inputSide;
	}
	
	public Side getInputSide(){
		return inputSide;
	}
	
	public void setLowOutputSide(Side outputSide){
		output0Side = outputSide;
	}
	
	public Side getLowOutputSide(){
		return output0Side;
	}
	
	public void setHighOutputSide(Side outputSide){
		output1Side = outputSide;
	}
	
	public Side getHighOutputSide(){
		return output1Side;
	}
}
