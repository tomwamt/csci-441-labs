package tomwamt.bluetech;

import java.util.Arrays;

public class BlueTechGame {
	private boolean[] inputValues;
	private FunctionBlock mainBlock;
	
	public void createAndGate(){
		inputValues = new boolean[]{true, true};
		
		mainBlock = new FunctionBlock("main", 2, 1);
		mainBlock.setInputs(inputValues);
		mainBlock.getInputBlock().setOutputSide(Block.Side.UP);
		mainBlock.getOutputBlock().setInputSide(Block.Side.DOWN);
		mainBlock.moveOutput(new Location(1, 4));
		
		Splitter splitter = new Splitter(1, 1);
		splitter.setInputSide(Block.Side.DOWN);
		splitter.setLowOutputSide(Block.Side.UP);
		splitter.setHighOutputSide(Block.Side.RIGHT);
		mainBlock.placeBlock(splitter, 0, 1);
		
		mainBlock.placeBlock(new Wire(1, Wire.WireFormat.HORIZONTAL), 1, 1);
		
		mainBlock.placeBlock(new Wire(1, Wire.WireFormat.TURN_UP_LEFT), 2, 1);
		
		DelayInverter inv1 = new DelayInverter(1, true);
		inv1.setInputSide(Block.Side.DOWN);
		inv1.setOutputSide(Block.Side.RIGHT);
		mainBlock.placeBlock(inv1, 0, 2);
		
		mainBlock.placeBlock(new Wire(1, Wire.WireFormat.T_UP), 1, 2);
		
		DelayInverter inv2 = new DelayInverter(1, true);
		inv2.setInputSide(Block.Side.DOWN);
		inv2.setOutputSide(Block.Side.LEFT);
		mainBlock.placeBlock(inv2, 2, 2);
		
		DelayInverter inv3 = new DelayInverter(1, true);
		inv3.setInputSide(Block.Side.DOWN);
		inv3.setOutputSide(Block.Side.UP);
		mainBlock.placeBlock(inv3, 1, 3);
		
		for(int i = 0; i < 10; i++){
			tick();
		}
	}
	
	public void tick(){
		mainBlock.update(null);
		mainBlock.tick();
	}
	
	public FunctionBlock getMainBlock(){
		return mainBlock;
	}
	
	public boolean[] getInputValues() {
		return inputValues;
	}

	public void setInputValues(boolean[] inputValues) {
		this.inputValues = inputValues;
	}

	public static void main(String[] args){
		BlueTechGame game = new BlueTechGame();
		game.createAndGate();
		System.out.println("inp "+Arrays.toString(game.mainBlock.getInputBlock().getOutput(Block.Side.UP)));
		System.out.println("out "+Arrays.toString(game.mainBlock.getOutput(game.mainBlock.getOutputSide())));
	}
}
