package tomwamt.bluetech;

public class OutputBlock extends Block{
	private int wireWidth;
	private Side inputSide;
	
	public OutputBlock(int wireWidth){
		setWireWidth(wireWidth);
		inputSide = Side.DOWN;
	}
	
	public void setWireWidth(int wireWidth){
		this.wireWidth = wireWidth;
		setValues(new boolean[wireWidth]);
	}
	
	public int getWireWidth(){
		return wireWidth;
	}
	
	@Override
	public void update(FunctionBlock parent) {
		setUpdated(true);
		boolean[] inputValues;
		Block inputBlock = parent.getBlock(getLocation().getAdjacent(inputSide));
		
		if(inputBlock != null && inputBlock.getOutputWireWidth(inputSide.opposite()) == wireWidth){
			if(!inputBlock.hasUpdated())
				inputBlock.update(parent);
			inputValues = inputBlock.getOutput(inputSide.opposite());
		}else{
			inputValues = new boolean[wireWidth];
		}
		
		setValues(inputValues);
	}
	
	@Override
	public void tick(){
		setUpdated(false);
	}
	
	public void setInputSide(Side inputSide){
		this.inputSide = inputSide;
	}
	
	public Side getInputSide(){
		return inputSide;
	}

	@Override
	public int getInputWireWidth(Side side) {
		if(side.equals(inputSide))
			return wireWidth;
		else
			return 0;
	}

	@Override
	public int getOutputWireWidth(Side side) {
		return 0;
	}

	@Override
	public boolean[] getOutput(Side side) {
		return null;
	}
}
