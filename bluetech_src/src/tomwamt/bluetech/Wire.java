package tomwamt.bluetech;

public class Wire extends Block{
	public enum WireFormat{
		HORIZONTAL, VERTICAL, // --, |
		TURN_UP_LEFT, TURN_UP_RIGHT, TURN_DOWN_LEFT, TURN_DOWN_RIGHT, // _|, |_, ...
		T_UP, T_DOWN, T_LEFT, T_RIGHT, // _|_, T, -|, |-
		CROSS // X
	}

	private int wireWidth;
	private WireFormat format;

	public Wire(int wireWidth, WireFormat format){
		setWireWidth(wireWidth);
		this.format = format;
	}

	public void setWireWidth(int wireWidth){
		this.wireWidth = wireWidth;
		setValues(new boolean[wireWidth]);
	}

	public int getWireWidth(){
		return wireWidth;
	}

	public void setFormat(WireFormat format){
		this.format = format;
	}

	public WireFormat getFormat(){
		return format;
	}

	@Override
	public void update(FunctionBlock parent) {
		setUpdated(true);
		// set everything to off
		for(int w = 0; w < wireWidth; w++)
			setValue(w, false);

		// iterate through non-wire inputs and updated wires
		for(Side side : Block.Side.values()){
			if(isConnected(side)){
				Block inputBlock = parent.getBlock(getLocation().getAdjacent(side));
				if(inputBlock != null && inputBlock.getOutputWireWidth(side.opposite()) == wireWidth){
					if(!(inputBlock instanceof Wire) || inputBlock.hasUpdated()){ // if not a wire, or if it is, it's updated
						if(!inputBlock.hasUpdated())
							inputBlock.update(parent);
						for(int w = 0; w < wireWidth; w++){
							if(inputBlock.getOutput(side.opposite())[w])
								setValue(w, true);
						}
					}
				}
			}
		}

		// iterate through non-updated wire inputs
		for(Side side : Block.Side.values()){
			if(isConnected(side)){
				Block inputBlock = parent.getBlock(getLocation().getAdjacent(side));
				if(inputBlock != null && inputBlock instanceof Wire && 
						inputBlock.getOutputWireWidth(side.opposite()) == wireWidth && !inputBlock.hasUpdated()){
					inputBlock.update(parent);
					for(int w = 0; w < wireWidth; w++){
						if(inputBlock.getOutput(side.opposite())[w])
							setValue(w, true);
					}
				}
			}
		}
	}

	@Override
	public void tick(){
		setUpdated(false);
	}

	public boolean isConnected(Side side) {
		switch(format){
		case CROSS:
			return true;
		case HORIZONTAL:
			switch(side){
			case UP:    return false;
			case DOWN:  return false;
			case LEFT:  return true;	
			case RIGHT: return true;
			}
		case T_DOWN:
			switch(side){
			case UP:    return false;
			case DOWN:  return true;
			case LEFT:  return true;	
			case RIGHT: return true;
			}
		case T_LEFT:
			switch(side){
			case UP:    return true;
			case DOWN:  return true;
			case LEFT:  return true;	
			case RIGHT: return false;
			}
		case T_RIGHT:
			switch(side){
			case UP:    return true;
			case DOWN:  return true;
			case LEFT:  return false;	
			case RIGHT: return true;
			}
		case T_UP:
			switch(side){
			case UP:    return true;
			case DOWN:  return false;
			case LEFT:  return true;	
			case RIGHT: return true;
			}
		case VERTICAL:
			switch(side){
			case UP:    return true;
			case DOWN:  return true;
			case LEFT:  return false;	
			case RIGHT: return false;
			}
		case TURN_DOWN_LEFT:
			switch(side){
			case UP:    return false;
			case DOWN:  return true;
			case LEFT:  return true;	
			case RIGHT: return false;
			}
		case TURN_DOWN_RIGHT:
			switch(side){
			case UP:    return false;
			case DOWN:  return true;
			case LEFT:  return false;	
			case RIGHT: return true;
			}
		case TURN_UP_LEFT:
			switch(side){
			case UP:    return true;
			case DOWN:  return false;
			case LEFT:  return true;	
			case RIGHT: return false;
			}
		case TURN_UP_RIGHT:
			switch(side){
			case UP:    return true;
			case DOWN:  return false;
			case LEFT:  return false;	
			case RIGHT: return true;
			}
		}
		return false;
	}

	@Override
	public int getInputWireWidth(Side side) {
		if(isConnected(side))
			return wireWidth;
		else
			return 0;
	}

	@Override
	public int getOutputWireWidth(Side side) {
		if(isConnected(side))
			return wireWidth;
		else
			return 0;
	}

	@Override
	public boolean[] getOutput(Side side) {
		if(isConnected(side))
			return getValues();
		else
			return null;
	}
	
	public boolean isOn(){
		return wireWidth == 1 && getValues()[0];
	}
}
