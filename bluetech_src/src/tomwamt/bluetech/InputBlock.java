package tomwamt.bluetech;

public class InputBlock extends Block {
	private int wireWidth;
	private Side outputSide;
	
	public InputBlock(int wireWidth){
		setWireWidth(wireWidth);
		outputSide = Side.UP;
	}
	
	public void setWireWidth(int wireWidth){
		this.wireWidth = wireWidth;
		setValues(new boolean[wireWidth]);
	}
	
	public int getWireWidth(){
		return wireWidth;
	}
	
	@Override
	public void update(FunctionBlock parent) {
		// values already set from parent
		setUpdated(true);
	}
	
	@Override
	public void tick(){
		setUpdated(false);
	}
	
	public void setOutputSide(Side outputSide){
		this.outputSide = outputSide;
	}
	
	public Side getOutputSide(){
		return outputSide;
	}

	@Override
	public int getInputWireWidth(Side side) {
		return 0;
	}

	@Override
	public int getOutputWireWidth(Side side) {
		if(side.equals(outputSide))
			return wireWidth;
		else
			return 0;
	}

	@Override
	public boolean[] getOutput(Side side) {
		if(side.equals(outputSide))
			return getValues();
		else
			return null;
	}
}
