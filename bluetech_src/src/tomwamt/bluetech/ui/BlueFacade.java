package tomwamt.bluetech.ui;

import java.util.HashMap;
import java.util.Map;

import tomwamt.bluetech.Block;
import tomwamt.bluetech.BlueTechGame;
import tomwamt.bluetech.DelayInverter;
import tomwamt.bluetech.FunctionBlock;
import tomwamt.bluetech.Wire;
import tomwamt.bluetech.Wire.WireFormat;

public class BlueFacade {
	private BlueTechGame game;
	private FunctionBlock currentFunc;
	private UIMain ui;
	
	private int clickMode;
	
	private Map<Block,BlockSprite> blockSprites = new HashMap<Block,BlockSprite>();
	
	public BlueFacade(UIMain ui){
		this.ui = ui;
		game = new BlueTechGame();
		game.createAndGate();
		currentFunc = game.getMainBlock();
	}
	
	public void createSprites(){
		for(Block block : currentFunc.getBlocks()){
			BlockSprite sprite = new BlockSprite(block);
			blockSprites.put(block, sprite);
			ui.getWindow().addSprite(sprite);
		}
	}
	
	public void tickGame(){
		game.tick();
	}
	
	public void setSelectedBlock(int x, int y){
		Block block = currentFunc.getBlock(x, y);
		if(block != null)
			System.out.println(block.getClass());
	}
	
	public void placeBlock(int x, int y){
		Block block = null;
		
		switch(clickMode){
		case 1:
			block = new Wire(1, WireFormat.CROSS);
			break;
		case 2:
			block = new DelayInverter(1, false);
			break;
		case 3:
			block = new DelayInverter(1, true);
			break;
		}
		
		if(block != null){
			if(currentFunc.placeBlock(block, x, y)){
				BlockSprite sprite = new BlockSprite(block);
				blockSprites.put(block, sprite);
				ui.getWindow().addSprite(sprite);
			}
		}
	}
	
	public void deleteBlock(int x, int y){
		Block block = currentFunc.getBlock(x, y);
		if(block != null && currentFunc.removeBlock(x, y)){
			BlockSprite sprite = blockSprites.remove(block);
			ui.getWindow().removeSprite(sprite);
		}
	}
	
	public boolean isBlockAt(int x, int y){
		return currentFunc.getBlock(x, y) != null;
	}
	
	public void setClickMode(int clickMode){
		this.clickMode = clickMode;
		System.out.println("Click mode set to "+clickMode);
	}
	
	public int getClickMode(){
		return clickMode;
	}
	
	public void adjustWidgets(){
		ui.adjustWidgets();
	}
}
