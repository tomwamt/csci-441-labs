package tomwamt.bluetech.ui;

import org.lwjgl.glfw.GLFW;

import tomwamt.engine.InputCallback;
import tomwamt.engine.MainWindow;

public class BlueInputCallback extends InputCallback {
	private BlueFacade facade;

	public BlueInputCallback(MainWindow window, BlueFacade facade) {
		super(window);
		this.facade = facade;
	}

	@Override
	public void keyPressed(int key, int mods) {
		if(key == GLFW.GLFW_KEY_SPACE){
			facade.tickGame();
		}
	}

	@Override
	public void keyReleased(int key, int mods) {
		
	}

	@Override
	public void mouseButtonPressed(int button) {
		if(button == 0){
			int x = (int)Math.floor(getWindow().getMouseWorldX() + 0.5f);
			int y = (int)Math.floor(getWindow().getMouseWorldY() + 0.5f);
			
			if(facade.getClickMode() == 0)
				facade.deleteBlock(x, y);
			else if(facade.isBlockAt(x, y))
				facade.setSelectedBlock(x, y);
			else
				facade.placeBlock(x, y);
		}
	}

	@Override
	public void mouseButtonReleased(int button) {
		
	}
}
