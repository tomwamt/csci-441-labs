package tomwamt.bluetech.ui;

import java.util.ArrayList;

import tomwamt.bluetech.Block;
import tomwamt.bluetech.Block.Side;
import tomwamt.bluetech.DelayInverter;
import tomwamt.bluetech.FunctionBlock;
import tomwamt.bluetech.Wire;
import tomwamt.engine.Sprite;

public class BlockSprite extends Sprite{
	private Block block;
	
	private ArrayList<Integer> textures;
	
	public BlockSprite(Block block){
		this.block = block;
		textures = new ArrayList<Integer>();
	}
	
	@Override
	public float getX(){
		return block.getLocation().x;
	}
	
	@Override
	public float getY(){
		return block.getLocation().y;
	}
	
	@Override
	public float getZ(){
		return 5f;
	}
	
	@Override
	public float getWidth(){
		return 1;
	}
	
	@Override
	public float getHeight(){
		return 1;
	}
	
	public float getRotation(){
		if(block instanceof Wire){
			Wire wire = (Wire) block;
			switch(wire.getFormat()){
			case TURN_DOWN_LEFT:
				return 180;
			case TURN_DOWN_RIGHT:
				return 90;
			case TURN_UP_RIGHT:
				return 270;
			case T_DOWN:
				return 180;
			case T_LEFT:
				return 90;
			case T_RIGHT:
				return 270;
			case VERTICAL:
				return 90;
			default:
				return 0;
			}
		}else
			return 0;
	}
	
	@Override
	public ArrayList<Integer> getTextures(){
		textures.clear();
		
		if(block instanceof Wire){
			Wire wire = (Wire) block;
			
			String suffix;
			if(wire.getWireWidth() > 1)
				suffix = "Multi.png";
			else if(wire.isOn())
				suffix = "On.png";
			else
				suffix = "Off.png";
			
			switch(wire.getFormat()){
			case CROSS:
				textures.add(Sprite.getTexture("res/wires/cross"+suffix));
				break;
			case TURN_DOWN_LEFT:
			case TURN_DOWN_RIGHT:
			case TURN_UP_RIGHT:
			case TURN_UP_LEFT:
				textures.add(Sprite.getTexture("res/wires/corner"+suffix));
				break;
			case T_DOWN:
			case T_LEFT:
			case T_RIGHT:
			case T_UP:
				textures.add(Sprite.getTexture("res/wires/t"+suffix));
				break;
			case VERTICAL:
			case HORIZONTAL:
				textures.add(Sprite.getTexture("res/wires/h"+suffix));
				break;
			}
		}else{
			textures.add(Sprite.getTexture("res/blocks/base.png"));
			
			if(block.getOutputWireWidth(Side.UP) > 0)
				textures.add(Sprite.getTexture("res/blocks/upOut.png"));
			if(block.getOutputWireWidth(Side.DOWN) > 0)
				textures.add(Sprite.getTexture("res/blocks/downpOut.png"));
			if(block.getOutputWireWidth(Side.LEFT) > 0)
				textures.add(Sprite.getTexture("res/blocks/leftOut.png"));
			if(block.getOutputWireWidth(Side.RIGHT) > 0)
				textures.add(Sprite.getTexture("res/blocks/rightOut.png"));
			
			if(block.getInputWireWidth(Side.UP) > 0)
				textures.add(Sprite.getTexture("res/blocks/upIn.png"));
			if(block.getInputWireWidth(Side.DOWN) > 0)
				textures.add(Sprite.getTexture("res/blocks/downIn.png"));
			if(block.getInputWireWidth(Side.LEFT) > 0)
				textures.add(Sprite.getTexture("res/blocks/leftIn.png"));
			if(block.getInputWireWidth(Side.RIGHT) > 0)
				textures.add(Sprite.getTexture("res/blocks/rightIn.png"));
			
			if(block instanceof FunctionBlock)
				textures.add(Sprite.getTexture("res/blocks/symbolFunc.png"));
			else if(block instanceof DelayInverter){
				DelayInverter inv = (DelayInverter) block;
				if(inv.isInverter())
					textures.add(Sprite.getTexture("res/blocks/symbolInvert.png"));
				else
					textures.add(Sprite.getTexture("res/blocks/symbolDelay.png"));
			}
		}
			
		return textures;
	}
}
