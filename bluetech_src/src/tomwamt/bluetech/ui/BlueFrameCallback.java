package tomwamt.bluetech.ui;

import org.lwjgl.glfw.GLFW;

import tomwamt.engine.Camera;
import tomwamt.engine.FrameCallback;
import tomwamt.engine.MainWindow;
import tomwamt.engine.Sprite;

public class BlueFrameCallback extends FrameCallback {
	private BlueFacade facade;
	private Sprite selectionBox;
	
	private float lastX;
	private float lastY;

	public BlueFrameCallback(MainWindow window, BlueFacade facade) {
		super(window);
		this.facade = facade;
		this.selectionBox = new Sprite("res/select.png");
		selectionBox.setZ(9.9f);
		
		window.addSprite(selectionBox);
	}

	@Override
	public void update(float delta) {
		facade.adjustWidgets();
		
		float speed = 4.0f;
		Camera cam = getWindow().getCamera();
		float x = cam.getX();
		float y = cam.getY();
		
		if(getWindow().keyHeld(GLFW.GLFW_KEY_W))
			y += speed*delta;
		if(getWindow().keyHeld(GLFW.GLFW_KEY_A))
			x -= speed*delta;
		if(getWindow().keyHeld(GLFW.GLFW_KEY_S))
			y -= speed*delta;
		if(getWindow().keyHeld(GLFW.GLFW_KEY_D))
			x += speed*delta;
		
		if(getWindow().mouseButtonHeld(1)){
			x -= (getWindow().getMouseX() - lastX)*delta*0.84;
			y += (getWindow().getMouseY() - lastY)*delta*0.84;
		}
		
		lastX = getWindow().getMouseX();
		lastY = getWindow().getMouseY();
		
		cam.setX(x);
		cam.setY(y);
		
		int sx = (int) Math.floor(getWindow().getMouseWorldX() + 0.5f);
		int sy = (int) Math.floor(getWindow().getMouseWorldY() + 0.5f);
		selectionBox.setPosition(sx, sy);
		
	}
}
