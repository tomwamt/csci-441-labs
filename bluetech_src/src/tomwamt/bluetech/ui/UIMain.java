package tomwamt.bluetech.ui;

import tomwamt.engine.MainWindow;
import tomwamt.engine.Widget;

public class UIMain {
	private MainWindow window;
	private BlueFacade facade;
	
	private Widget leftFrame;
	private Widget deleteButton;
	private Widget wireButton;
	private Widget delayButton;
	private Widget invertButton;
	
	private Widget rightFrame;
	
	public UIMain(){
		facade = new BlueFacade(this);
		window = new MainWindow("BlueTech", 1280, 720);
		window.setFrameCallback(new BlueFrameCallback(window, facade));
		window.setInputCallback(new BlueInputCallback(window, facade));
		
		window.getCamera().setSize(10);
		
		facade.createSprites();
		
		leftFrame = new Widget(0, 0, 128, 500, "res/ui/frame.png");
		window.addWidget(leftFrame);
		
		deleteButton = new Widget(32, 0, 64, 64, "res/ui/delete.png"){
			@Override
			public boolean isClickable(){
				return true;
			}
			
			@Override
			public void onClick(){
				facade.setClickMode(0);
			}
		};
		window.addWidget(deleteButton);
		
		wireButton = new Widget(32, 0, 64, 64, "res/ui/wire.png"){
			@Override
			public boolean isClickable(){
				return true;
			}
			
			@Override
			public void onClick(){
				facade.setClickMode(1);
			}
		};
		window.addWidget(wireButton);
		
		delayButton = new Widget(32, 0, 64, 64, "res/ui/delay.png"){
			@Override
			public boolean isClickable(){
				return true;
			}
			
			@Override
			public void onClick(){
				facade.setClickMode(2);
			}
		};
		window.addWidget(delayButton);
		
		invertButton = new Widget(32, 0, 64, 64, "res/ui/invert.png"){
			@Override
			public boolean isClickable(){
				return true;
			}
			
			@Override
			public void onClick(){
				facade.setClickMode(3);
			}
		};
		window.addWidget(invertButton);
		
		rightFrame = new Widget(0, 0, 128, 500, "res/ui/frame.png");
		window.addWidget(rightFrame);
		
		window.run();
	}
	
	public void adjustWidgets(){
		int frameTop = window.getHeight()/2 - 250;
		leftFrame.setY(frameTop);
		deleteButton.setY(frameTop + 32);
		wireButton.setY(frameTop + 128);
		delayButton.setY(frameTop + 224);
		invertButton.setY(frameTop + 320);
		
		rightFrame.setX(window.getWidth()-128);
		rightFrame.setY(frameTop);
	}
	
	public MainWindow getWindow(){
		return window;
	}
	
	public static void main(String[] args){
		new UIMain();
	}
}
