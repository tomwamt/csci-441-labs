package tomwamt.bluetech;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Queue;

public class DelayInverter extends Block {
	private int wireWidth;
	private Side inputSide;
	private Side outputSide;
	private boolean invert;
	private int delay;
	
	private Queue<boolean[]> queuedValues;
	
	private FunctionBlock parent;
	
	public DelayInverter(int wireWidth, boolean invert){
		setWireWidth(wireWidth);
		this.invert = invert;
		setDelay(1);
		inputSide = Side.DOWN;
		outputSide = Side.UP;
	}
	
	public void setWireWidth(int wireWidth){
		this.wireWidth = wireWidth;
		boolean[] init = new boolean[wireWidth];
		Arrays.fill(init, true);
		setValues(init);
	}

	@Override
	public void update(FunctionBlock parent) {
		if(this.parent == null){
			this.parent = parent;
		}
		
		setUpdated(true);
		setValues(queuedValues.poll());
		
		Block inputBlock = parent.getBlock(getLocation().getAdjacent(inputSide));
		
		if(inputBlock != null && inputBlock.getOutputWireWidth(inputSide.opposite()) == wireWidth && !inputBlock.hasUpdated())
			inputBlock.update(parent);
	}
	
	@Override
	public void tick(){
		boolean[] inputValues;
		
		Block inputBlock = parent.getBlock(getLocation().getAdjacent(inputSide));
		
		if(inputBlock != null && inputBlock.getOutputWireWidth(inputSide.opposite()) == wireWidth)
			inputValues = inputBlock.getOutput(inputSide.opposite()).clone();
		else
			inputValues = new boolean[wireWidth];
		
		if(invert){
			for(int w = 0; w < wireWidth; w++)
				inputValues[w] = !inputValues[w];
		}
		
		queuedValues.offer(inputValues);
		
		setUpdated(false);
	}

	@Override
	public int getInputWireWidth(Side side) {
		if(side.equals(inputSide))
			return wireWidth;
		else
			return 0;
	}

	@Override
	public int getOutputWireWidth(Side side) {
		if(side.equals(outputSide))
			return wireWidth;
		else
			return 0;
	}

	public void setInputSide(Side inputSide){
		if(!inputSide.equals(outputSide))
			this.inputSide = inputSide;
	}
	
	public Side getInputSide(){
		return inputSide;
	}
	
	public void setOutputSide(Side outputSide){
		if(!outputSide.equals(inputSide))
			this.outputSide = outputSide;
	}
	
	public Side getOutputSide(){
		return outputSide;
	}
	
	public void setDelay(int delay){
		if(delay > 0){
			this.delay = delay;
			queuedValues = new ArrayDeque<boolean[]>();
			for(int i = 0; i < delay; i++){
				queuedValues.offer(getValues());
			}
		}
	}
	
	public int getDelay(){
		return delay;
	}
	
	public void setInverter(boolean invert){
		this.invert = invert;
	}
	
	public boolean isInverter(){
		return invert;
	}

	@Override
	public boolean[] getOutput(Side side) {
		if(side.equals(outputSide))
			return getValues();
		else
			return null;
	}
}
