package tomwamt.bluetech;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class FunctionBlock extends Block{
	private boolean[] mainInputs;
	
	private Map<Location, Block> grid;
	
	private InputBlock input;
	private Side inputSide;
	private int inputWidth;
	
	private OutputBlock output;
	private Side outputSide;
	private int outputWidth;
	
	private String name;
	
	public FunctionBlock(String name, int inputWidth, int outputWidth){
		grid = new HashMap<Location, Block>();
		this.name = name;
		
		this.inputWidth = inputWidth;
		this.outputWidth = outputWidth;
		
		input = new InputBlock(inputWidth);
		input.setLocation(new Location(0, 0));
		grid.put(input.getLocation(), input);
		output = new OutputBlock(outputWidth);
		output.setLocation(new Location(0, 1));
		grid.put(output.getLocation(), output);
		
		inputSide = Side.DOWN;
		outputSide = Side.UP;
	}
	
	public boolean removeBlock(Block block){
		if(grid.containsValue(block))
			return removeBlock(block.getLocation());
		return false;
	}
	
	public boolean removeBlock(int x, int y){
		return removeBlock(new Location(x, y));
	}
	public boolean removeBlock(Location loc){
		return placeBlock(null, loc);
	}
	
	public boolean placeBlock(Block block, int x, int y){
		return placeBlock(block, new Location(x, y));
	}
	public boolean placeBlock(Block block, Location loc){
		if(loc.equals(input.getLocation()) || loc.equals(output.getLocation())){
			return false;
		}else{
			if(block == null){
				if(getBlock(loc) == null)
					return false;
				else
					grid.remove(loc);
			}else{
				grid.put(loc, block);
				block.setLocation(loc);
			}
			return true;
		}
	}
	
	public Block getBlock(int x, int y){
		return getBlock(new Location(x, y));
	}
	public Block getBlock(Location loc){
		return grid.get(loc);
	}
	
	public void moveBlock(Location oldLoc, Location newLoc){
		Block old = getBlock(oldLoc);
		if(old != null){
			if(placeBlock(old, newLoc))
				removeBlock(oldLoc);
		}
	}
	
	public void moveInput(Location newLoc){
		moveBlock(input.getLocation(), newLoc);
	}
	
	public void moveOutput(Location newLoc){
		moveBlock(output.getLocation(), newLoc);
	}
	
	@Override
	public void update(FunctionBlock parent){
		setUpdated(true);
		boolean[] inputValues;
		if(parent != null){ // Read input from input block in parent grid
			Block inputBlock = parent.getBlock(getLocation().getAdjacent(inputSide));
			
			if(inputBlock != null && inputBlock.getOutputWireWidth(inputSide.opposite()) == inputWidth){
				if(!inputBlock.hasUpdated())
					inputBlock.update(parent);
				inputValues = inputBlock.getOutput(inputSide.opposite());
			}else{
				inputValues = new boolean[inputWidth];
			}
		}else{ // Read input from main class
			inputValues = mainInputs;
		}
		
		input.setValues(inputValues);
		output.update(this);
		setValues(output.getValues());
	}
	
	@Override
	public void tick(){
		setUpdated(false);
		for(Block block : grid.values()){
			if(block.hasUpdated())
				block.tick();
		}
	}
	
	public String getName(){
		return name;
	}
	
	public void setInputSide(Side inputSide){
		if(!inputSide.equals(outputSide))
			this.inputSide = inputSide;
	}
	
	public Side getInputSide(){
		return inputSide;
	}
	
	public void setOutputSide(Side outputSide){
		if(!outputSide.equals(inputSide))
			this.outputSide = outputSide;
	}
	
	public Side getOutputSide(){
		return outputSide;
	}

	@Override
	public int getInputWireWidth(Side side) {
		if(side.equals(inputSide))
			return inputWidth;
		else
			return 0;
	}

	@Override
	public int getOutputWireWidth(Side side) {
		if(side.equals(outputSide))
			return outputWidth;
		else
			return 0;
	}
	
	public InputBlock getInputBlock(){
		return input;
	}
	
	public OutputBlock getOutputBlock(){
		return output;
	}

	@Override
	public boolean[] getOutput(Side side) {
		if(side.equals(outputSide))
			return getValues();
		else
			return null;
	}
	
	public Collection<Block> getBlocks(){
		return grid.values();
	}
	
	public void setInputs(boolean[] inputs){
		mainInputs = inputs;
	}
}
