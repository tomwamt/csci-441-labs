package tomwamt.bluetech;

public abstract class Block {
	public enum Side{
		UP, DOWN, LEFT, RIGHT;
		
		public Side opposite(){
			switch(this){
			case UP: return DOWN;
			case DOWN: return UP;
			case LEFT: return RIGHT;
			case RIGHT: return LEFT;
			default: return null;
			}
		}
	}
	
	private Location loc;
	private boolean[] values;
	private boolean updated = false;

	public abstract void update(FunctionBlock parent); // called on blocks in a tree to change output values (usually based on input)
	public abstract void tick(); // called on every block in arbitrary order after update, does not change block output values
	public abstract int getInputWireWidth(Side side);
	public abstract int getOutputWireWidth(Side side);
	public abstract boolean[] getOutput(Side side);
	
	public void setLocation(Location loc){
		this.loc = loc;
	}
	
	public Location getLocation(){
		return loc;
	}
	
	protected void setValue(int wire, boolean value){
		values[wire] = value;
	}
	
	protected void setValues(boolean[] values){
		if(this.values == null)
			this.values = new boolean[values.length];
		if(this.values.length == values.length){
			for(int i = 0; i < values.length; i++)
				this.values[i] = values[i];
		}else{
			this.values = values.clone();
		}
	}
	
	protected boolean[] getValues(){
		return values;
	}
	
	protected void setUpdated(boolean updated){
		this.updated = updated;
	}
	
	protected boolean hasUpdated(){
		return updated;
	}
}
