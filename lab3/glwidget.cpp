#include "glwidget.h"
#include <iostream>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <QTextStream>

using namespace std;


GLWidget::GLWidget(QWidget *parent) : QOpenGLWidget(parent), outline(false), drawMode(0) {
	drawMode = GL_POINTS;
}

GLWidget::~GLWidget() {
}

void GLWidget::keyPressEvent(QKeyEvent *event) {
	switch(event->key()) {
		case Qt::Key_C:
			cout << "Cleared all the points." << endl;
			
			pts.clear();

			glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
			glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_DYNAMIC_DRAW);
			break;
		case Qt::Key_W:
			outline = !outline;
			if(outline) {
				cout << "Displaying outlines." << endl;
				glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			} else {
				cout << "Displaying filled polygons." << endl;
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			}
			break;
		case Qt::Key_Space:
			switch(drawMode){
			case GL_POINTS:
				drawMode = GL_LINES;
				break;
			case GL_LINES:
				drawMode = GL_LINE_LOOP;
				break;
			case GL_LINE_LOOP:
				drawMode = GL_LINE_STRIP;
				break;
			case GL_LINE_STRIP:
				drawMode = GL_TRIANGLES;
				break;
			case GL_TRIANGLES:
				drawMode = GL_TRIANGLE_STRIP;
				break;
			case GL_TRIANGLE_STRIP:
				drawMode = GL_TRIANGLE_FAN;
				break;
			case GL_TRIANGLE_FAN:
				drawMode = GL_POINTS;
				break;
			}
			
			cout << "Changed draw mode to " << drawMode << endl;
			break;
	}
	update();
}

void GLWidget::mousePressEvent(QMouseEvent *event) {
	vec2 pt;
	pt.x = event->x();
	pt.y = event->y();
	pts.push_back(pt);

	cout << "Added point (" << pt.x << ", " << pt.y << ") " << endl;

	glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
	glBufferData(GL_ARRAY_BUFFER, pts.size()*sizeof(vec2), pts.data(), GL_DYNAMIC_DRAW);
	update();
}

void GLWidget::initializeGL() {
	initializeOpenGLFunctions();

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glPointSize(4.0f);

	// Create a new Vertex Array Object on the GPU which
	// saves the attribute layout of our vertices.
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// Create a buffer on the GPU for position data
	glGenBuffers(1, &positionBuffer);

	// Upload the position data to the GPU, storing
	// it in the buffer we just allocated.
	glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
	glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_DYNAMIC_DRAW);

	// Load our vertex and fragment shaders into a program object
	// on the GPU
	program = loadShaders(":/vert.glsl", ":/frag.glsl");

	// Bind the attribute "position" (defined in our vertex shader)
	// to the currently bound buffer object, which contains our
	// position data for a single triangle. This information 
	// is stored in our vertex array object.
	glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
	GLint positionIndex = glGetAttribLocation(program, "position");
	glEnableVertexAttribArray(positionIndex);
	glVertexAttribPointer(positionIndex, 2, GL_FLOAT, GL_FALSE, 0, 0);
	
	proj = glm::ortho(0.0f, 640.0f, 480.0f, 0.0f);
	
}

void GLWidget::resizeGL(int w, int h) {
	glViewport(0,0,w,h);

	float wf = w;
	float hf = h;
	proj = glm::ortho(0.0f, wf, hf, 0.0f);
}

void GLWidget::paintGL() {
	glClear(GL_COLOR_BUFFER_BIT);
	
	glUseProgram(program);
	
	GLuint matrixIndex = glGetUniformLocation(program, "projection");
	glUniformMatrix4fv(matrixIndex, 1, GL_FALSE, &proj[0][0]);
	
	glBindVertexArray(vao);

	// draw primitives based on the current draw mode
	glDrawArrays(drawMode, 0, pts.size());
	
	// draw points so we can always see them
	// glPointSize adjusts the size of point
	// primitives
	glDrawArrays(GL_POINTS, 0, pts.size());
}

// Copied from LoadShaders.cpp in the the oglpg-8th-edition.zip
// file provided by the OpenGL Programming Guide, 8th edition.
const GLchar* GLWidget::readShader(const char* filename) {
#ifdef WIN32
		FILE* infile;
		fopen_s( &infile, filename, "rb" );
#else
	FILE* infile = fopen( filename, "rb" );
#endif // WIN32

	if ( !infile ) {
#ifdef _DEBUG
		std::cerr << "Unable to open file '" << filename << "'" << std::endl;
#endif /* DEBUG */
		return NULL;
	}

	fseek( infile, 0, SEEK_END );
	int len = ftell( infile );
	fseek( infile, 0, SEEK_SET );

	GLchar* source = new GLchar[len+1];

	fread( source, 1, len, infile );
	fclose( infile );

	source[len] = 0;

	return const_cast<const GLchar*>(source);
}

GLuint GLWidget::loadShaders(const char* vertf, const char* fragf) {
	GLuint program = glCreateProgram();

	// read vertex shader from Qt resource file
	QFile vertFile(vertf);
	vertFile.open(QFile::ReadOnly | QFile::Text);
	QString vertString;
	QTextStream vertStream(&vertFile);
	vertString.append(vertStream.readAll());
	std::string vertSTLString = vertString.toStdString();

	const GLchar* vertSource = vertSTLString.c_str();

	GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertShader, 1, &vertSource, NULL);
	glCompileShader(vertShader);
	{
		GLint compiled;
		glGetShaderiv( vertShader, GL_COMPILE_STATUS, &compiled );
		if ( !compiled ) {
			GLsizei len;
			glGetShaderiv( vertShader, GL_INFO_LOG_LENGTH, &len );

			GLchar* log = new GLchar[len+1];
			glGetShaderInfoLog( vertShader, len, &len, log );
			std::cout << "Shader compilation failed: " << log << std::endl;
			delete [] log;
		}
	}
	glAttachShader(program, vertShader);

	// read fragment shader from Qt resource file
	QFile fragFile(fragf);
	fragFile.open(QFile::ReadOnly | QFile::Text);
	QString fragString;
	QTextStream fragStream(&fragFile);
	fragString.append(fragStream.readAll());
	std::string fragSTLString = fragString.toStdString();

	const GLchar* fragSource = fragSTLString.c_str();

	GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragShader, 1, &fragSource, NULL);
	glCompileShader(fragShader);
	{
		GLint compiled;
		glGetShaderiv( fragShader, GL_COMPILE_STATUS, &compiled );
		if ( !compiled ) {
			GLsizei len;
			glGetShaderiv( fragShader, GL_INFO_LOG_LENGTH, &len );

			GLchar* log = new GLchar[len+1];
			glGetShaderInfoLog( fragShader, len, &len, log );
			std::cerr << "Shader compilation failed: " << log << std::endl;
			delete [] log;
		}
	}
	glAttachShader(program, fragShader);

	glLinkProgram(program);
	{
		GLint linked;
		glGetProgramiv( program, GL_LINK_STATUS, &linked );
		if ( !linked ) {
			GLsizei len;
			glGetProgramiv( program, GL_INFO_LOG_LENGTH, &len );

			GLchar* log = new GLchar[len+1];
			glGetProgramInfoLog( program, len, &len, log );
			std::cout << "Shader linker failed: " << log << std::endl;
			delete [] log;
		}
	}

	return program;
}

