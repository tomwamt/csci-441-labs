
#include <iostream>
#include <string>

using namespace std;

class Vector3{
public:
	float x, y, z;

	Vector3() : x(0), y(0), z(0){
		cout << "empty Vector3 constructed" << endl;
	}

	Vector3(float xx, float yy, float zz) : x(xx), y(yy), z(zz){
		cout << "Vector3 constructed" << endl;
	}

	~Vector3(){
		cout << "Vector3 destroyed" << endl;
	}

	Vector3 operator+(const Vector3& b){
		float xx = x + b.x;
		float yy = y + b.y;
		float zz = z + b.z;
		return Vector3(xx, yy, zz);
	}
};

ostream& operator<<(ostream& stream, const Vector3& v){
	stream << "<" << v.x << ", " << v.y << ", " << v.z << ">";
	return stream;
}

int main(int argc, char** argv){
	for(int i = 0; i < argc; i++){
		cout << argv[i] << endl;
	}

	cout << "What is your name?" << endl << ">> ";
	string name;
	cin >> name;
	cout << "Hello " << name << endl;

	Vector3 a(1, 2, 3);
	Vector3 b(4, 5, 6);
	cout << a+b << endl;

	Vector3 c; // Allocated to stack
	c.y = 5;
	cout << c << endl;

	Vector3* cp = new Vector3(); // Allocated to heap
	cp->y = 5;
	cout << *cp << endl;
	delete cp;

	Vector3 arr[10]; // Allocated to stack, constant size

	cout << "Enter a number of vectors to create:" << endl << ">> ";
	int n;
	cin >> n;
	Vector3* arr2 = new Vector3[n]; // Allocated to heap, dynamic size
	for(int i = 0; i < n; i++){
		arr2[i].y = i;
	}
	for(int i = 0; i < n; i++){
		cout << arr2[i] << endl;
	}
	delete [] arr2;

	cout << "done" << endl;
	return 0;
}
