#include <QApplication>
#include <QOpenGLFunctions_3_3_Core>

#include "glwidget.h"
#include <iostream>

using namespace std;

struct Info : QOpenGLFunctions_3_3_Core {
    void printOpenGLInfo();
};

void Info::printOpenGLInfo() {
    initializeOpenGLFunctions();

    const GLubyte* version = glGetString(GL_VERSION);
    cout << version << endl;

    const GLubyte* vendor = glGetString(GL_VENDOR);
    cout << vendor << endl;

    const GLubyte* renderer = glGetString(GL_RENDERER);
    cout << renderer << endl;

    const GLubyte* shaderVersion = glGetString(GL_SHADING_LANGUAGE_VERSION);
    cout << shaderVersion << endl;

    int numExtensions;
    glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);

    for(int i = 0; i < numExtensions; i++) {
        const GLubyte* extension = glGetStringi(GL_EXTENSIONS,i);
        cout << extension << endl;
    }
}

int main(int argc, char** argv) {
	cout << "Enter 3 points as x,y:r,g,b" << endl;
	char ch;
	
	Point pt1;
	Color clr1;
	cout << "> ";
	cin >> pt1.x >> ch >> pt1.y >> ch >> clr1.r >> ch >> clr1.g >> ch >> clr1.b;
	
	Point pt2;
	Color clr2;
	cout << "> ";
	cin >> pt2.x >> ch >> pt2.y >> ch >> clr2.r >> ch >> clr2.g >> ch >> clr2.b;
	
	Point pt3;
	Color clr3;
	cout << "> ";
	cin >> pt3.x >> ch >> pt3.y >> ch >> clr3.r >> ch >> clr3.g >> ch >> clr3.b;
	
	cout << "You entered: " << endl;
	cout << pt1.x << ", " << pt1.y << endl;
	cout << pt2.x << ", " << pt2.y << endl;
	cout << pt3.x << ", " << pt3.y << endl;
	
    QApplication a(argc, argv);

    QSurfaceFormat format;
    format.setVersion(3,3);  // set a modern version of OpenGL
    format.setProfile(QSurfaceFormat::CoreProfile); 
    QSurfaceFormat::setDefaultFormat(format);

    GLWidget glWidget;
    glWidget.mypts[0] = pt1;
    glWidget.mypts[1] = pt2;
    glWidget.mypts[2] = pt3;
    glWidget.mycolors[0] = clr1;
    glWidget.mycolors[1] = clr2;
    glWidget.mycolors[2] = clr3;
    glWidget.resize(640,480);
    glWidget.show();

    Info f;
    f.printOpenGLInfo();

    return a.exec();
}
