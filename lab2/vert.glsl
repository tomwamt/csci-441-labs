#version 330

in vec2 position;
in vec3 color;

out vec3 color_f;

void main() {
  gl_Position = vec4(position.x, position.y, 0, 1);
  color_f = color;
}
