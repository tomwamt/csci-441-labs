#version 330

in vec3 color_f;

out vec4 color_out;

void main() {
  color_out = vec4(color_f, 1.0);
}
