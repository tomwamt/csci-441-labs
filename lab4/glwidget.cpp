#include "glwidget.h"
#include <iostream>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <QTextStream>

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

using glm::vec3;
using glm::vec4;
using glm::value_ptr;
using glm::ortho;
using glm::mat4;
using glm::cos;
using glm::sin;
using glm::normalize;
using glm::cross;
using glm::inverse;

GLWidget::GLWidget(QWidget *parent) : QOpenGLWidget(parent) { 
    tx = 0;
    ty = 0;
    tz = 0;

    sx = 1;
    sy = 1;
    sz = 1;

    rx = 0;
    ry = 0;
    rz = 0;

    camAngle = 0;
}

GLWidget::~GLWidget() {
}

void GLWidget::initializeGrid() {
    glGenVertexArrays(1, &gridVao);
    glBindVertexArray(gridVao);

    // Create a buffer on the GPU for position data
    GLuint positionBuffer;
    glGenBuffers(1, &positionBuffer);

    vec3 pts[84];
    for(int i = -10; i <= 10; i++) {

        pts[2*(i+10)] = vec3(i, -.5f, 10);
        pts[2*(i+10)+1] = vec3(i, -.5f, -10);

        pts[2*(i+10)+42] = vec3(10,-.5f, i);
        pts[2*(i+10)+43] = vec3(-10,-.5f, i);
    }

    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pts), pts, GL_STATIC_DRAW);

    // Load our vertex and fragment shaders into a program object
    // on the GPU
    GLuint program = loadShaders(":/grid_vert.glsl", ":/grid_frag.glsl");
    glUseProgram(program);
    gridProg = program;

    // Bind the attribute "position" (defined in our vertex shader)
    // to the currently bound buffer object, which contains our
    // position data for a single triangle. This information 
    // is stored in our vertex array object.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    GLint positionIndex = glGetAttribLocation(program, "position");
    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    gridProjMatrixLoc = glGetUniformLocation(program, "projection");
    gridViewMatrixLoc = glGetUniformLocation(program, "view");
}

void GLWidget::initializeCube() {
    // Create a new Vertex Array Object on the GPU which
    // saves the attribute layout of our vertices.
    glGenVertexArrays(1, &cubeVao);
    glBindVertexArray(cubeVao);

    // Create a buffer on the GPU for position data
    GLuint positionBuffer;
    glGenBuffers(1, &positionBuffer);

    GLuint colorBuffer;
    glGenBuffers(1, &colorBuffer);

    GLuint indexBuffer;
    glGenBuffers(1, &indexBuffer);

    vec3 pts[] = {
        // top
        vec3(1,1,1),    // 0
        vec3(1,1,-1),   // 1
        vec3(-1,1,-1),  // 2
        vec3(-1,1,1),   // 3

        // bottom
        vec3(1,-1,1),   // 4
        vec3(-1,-1,1),  // 5
        vec3(-1,-1,-1), // 6
        vec3(1,-1,-1),  // 7

        // front
        vec3(1,1,1),    // 8
        vec3(-1,1,1),   // 9
        vec3(-1,-1,1),  // 10
        vec3(1,-1,1),   // 11

        // back
        vec3(-1,-1,-1), // 12
        vec3(-1,1,-1),  // 13
        vec3(1,1,-1),   // 14
        vec3(1,-1,-1),  // 15

        // right
        vec3(1,-1,1),   // 16
        vec3(1,-1,-1),  // 17
        vec3(1,1,-1),   // 18
        vec3(1,1,1),     // 19

        // left
        vec3(-1,-1,1),  // 20
        vec3(-1,1,1),   // 21
        vec3(-1,1,-1),  // 22
        vec3(-1,-1,-1) // 23

    };

    for(int i = 0; i < 24; i++) {
        pts[i] *= .5;
    }

    vec3 colors[] = {
        // top
        vec3(0,1,0),    
        vec3(0,1,0),    
        vec3(0,1,0),    
        vec3(0,1,0),    

        // bottom
        vec3(0,.5f,0),  
        vec3(0,.5f,0),  
        vec3(0,.5f,0),  
        vec3(0,.5f,0),  

        // front
        vec3(0,0,1),    
        vec3(0,0,1),    
        vec3(0,0,1),    
        vec3(0,0,1),    

        // back
        vec3(0,0,.5f),  
        vec3(0,0,.5f),  
        vec3(0,0,.5f),  
        vec3(0,0,.5f),

        // right
        vec3(1,0,0),    
        vec3(1,0,0),    
        vec3(1,0,0),    
        vec3(1,0,0),    


        // left
        vec3(.5f,0,0),  
        vec3(.5f,0,0),  
        vec3(.5f,0,0),  
        vec3(.5f,0,0)  
    };

    GLuint restart = 0xFFFFFFFF;
    GLuint indices[] = {
        0,1,2,3, restart,
        4,5,6,7, restart,
        8,9,10,11, restart,
        12,13,14,15, restart,
        16,17,18,19, restart,
        20,21,22,23
    };

    // Upload the position data to the GPU, storing
    // it in the buffer we just allocated.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pts), pts, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(colors), colors, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    // Load our vertex and fragment shaders into a program object
    // on the GPU
    GLuint program = loadShaders(":/vert.glsl", ":/frag.glsl");
    glUseProgram(program);
    cubeProg = program;

    // Bind the attribute "position" (defined in our vertex shader)
    // to the currently bound buffer object, which contains our
    // position data for a single triangle. This information 
    // is stored in our vertex array object.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    GLint positionIndex = glGetAttribLocation(program, "position");
    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
    GLint colorIndex = glGetAttribLocation(program, "color");
    glEnableVertexAttribArray(colorIndex);
    glVertexAttribPointer(colorIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    cubeProjMatrixLoc = glGetUniformLocation(program, "projection");
    cubeViewMatrixLoc = glGetUniformLocation(program, "view");
    cubeModelMatrixLoc = glGetUniformLocation(program, "model");
}

void GLWidget::initializeGL() {
    initializeOpenGLFunctions();

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glPointSize(4.0f);

    glEnable(GL_DEPTH_TEST);
    GLuint restart = 0xFFFFFFFF;
    glPrimitiveRestartIndex(restart);
    glEnable(GL_PRIMITIVE_RESTART);

    initializeCube();
    initializeGrid();
}

void GLWidget::resizeGL(int w, int h) {
    glViewport(0,0,w,h);
    
    windowWidth = w;
    windowHeight = h;

    updateProjMatrix(w, h);
}

void GLWidget::paintGL() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glUseProgram(cubeProg);
    glUniformMatrix4fv(cubeViewMatrixLoc, 1, GL_FALSE, value_ptr(viewMatrix));
    glUniformMatrix4fv(cubeModelMatrixLoc, 1, GL_FALSE, value_ptr(modelMatrix));
    
    glUseProgram(gridProg);
    glUniformMatrix4fv(gridViewMatrixLoc, 1, GL_FALSE, value_ptr(viewMatrix));

    renderGrid();
    renderCube();
}

void GLWidget::renderCube() {
    glUseProgram(cubeProg);
    glBindVertexArray(cubeVao);
    glDrawElements(GL_TRIANGLE_FAN, 29, GL_UNSIGNED_INT, 0);
}

void GLWidget::renderGrid() {
    glUseProgram(gridProg);
    glBindVertexArray(gridVao);
    glDrawArrays(GL_LINES, 0, 84);
}

GLuint GLWidget::loadShaders(const char* vertf, const char* fragf) {
    GLuint program = glCreateProgram();

    // read vertex shader from Qt resource file
    QFile vertFile(vertf);
    vertFile.open(QFile::ReadOnly | QFile::Text);
    QString vertString;
    QTextStream vertStream(&vertFile);
    vertString.append(vertStream.readAll());
    std::string vertSTLString = vertString.toStdString();

    const GLchar* vertSource = vertSTLString.c_str();

    GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertShader, 1, &vertSource, NULL);
    glCompileShader(vertShader);
    {
        GLint compiled;
        glGetShaderiv( vertShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( vertShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( vertShader, len, &len, log );
            std::cout << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, vertShader);

    // read fragment shader from Qt resource file
    QFile fragFile(fragf);
    fragFile.open(QFile::ReadOnly | QFile::Text);
    QString fragString;
    QTextStream fragStream(&fragFile);
    fragString.append(fragStream.readAll());
    std::string fragSTLString = fragString.toStdString();

    const GLchar* fragSource = fragSTLString.c_str();

    GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragShader, 1, &fragSource, NULL);
    glCompileShader(fragShader);
    {
        GLint compiled;
        glGetShaderiv( fragShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( fragShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( fragShader, len, &len, log );
            std::cerr << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, fragShader);

    glLinkProgram(program);

    return program;
}

void GLWidget::translateX(int x) {
    tx = (float)x/10;
    std::cout << "Translate X " << tx << std::endl;
    updateModelMatrix();
}

void GLWidget::translateY(int y) {
    ty = (float)y/10;
    std::cout << "Translate Y " << ty << std::endl;
    updateModelMatrix();
}

void GLWidget::translateZ(int z) {
    tz = (float)z/10;
    std::cout << "Translate Z " << tz << std::endl;
    updateModelMatrix();
}

void GLWidget::rotateX(int x) {
    std::cout << "Rotate X " << x << std::endl;
    rx = (float)x*M_PI/180;
    updateModelMatrix();
}

void GLWidget::rotateY(int y) {
    std::cout << "Rotate Y " << y << std::endl;
    ry = (float)y*M_PI/180;
    updateModelMatrix();
}

void GLWidget::rotateZ(int z) {
    std::cout << "Rotate Z " << z << std::endl;
    rz = (float)z*M_PI/180;
    updateModelMatrix();
}

void GLWidget::scaleX(int x) {
    sx = (float)x/10;
    std::cout << "Scale X " << sx << std::endl;
    updateModelMatrix();
}

void GLWidget::scaleY(int y) {
    sy = (float)y/10;
    std::cout << "Scale Y " << sy << std::endl;
    updateModelMatrix();
}

void GLWidget::scaleZ(int z) {
    sz = (float)z/10;
    std::cout << "Scale Z " << sz << std::endl;
    updateModelMatrix();
}

void GLWidget::cameraPosition(int angle) {
    camAngle = angle*M_PI/180;
    std::cout << "Camera angle " << angle << std::endl;
    updateViewMatrix();
}

void GLWidget::cameraY(int y) {
    camY = (float)y/10;
    std::cout << "Camera Y " << y << std::endl;
    updateViewMatrix();
}

void GLWidget::cameraZoom(int zoom){
	cameraFov = (float)zoom;
	std::cout << "Camera FOV " << zoom << std::endl;
	updateProjMatrix(windowWidth, windowHeight);
}

void GLWidget::updateModelMatrix() {
    // Part 3 - Construct a combined model matrix from the translation,
    // rotation and scale values and upload it as a uniform variable to
    // the cube program (don't use it to change the grid). Update your
    // vertex shader accordingly.
    
    mat4 trans = mat4(1.0f);
    trans[3][0] = tx;
    trans[3][1] = ty;
    trans[3][2] = tz;
    
    mat4 rotX = mat4(1.0f);
    rotX[1][1] = cos(rx);
    rotX[1][2] = sin(rx);
    rotX[2][1] = -sin(rx);
    rotX[2][2] = cos(rx);
    
    mat4 rotY = mat4(1.0f);
    rotY[0][0] = cos(ry);
    rotY[2][0] = sin(ry);
    rotY[0][2] = -sin(ry);
    rotY[2][2] = cos(ry);
    
    mat4 rotZ = mat4(1.0f);
    rotZ[0][0] = cos(rz);
    rotZ[0][1] = sin(rz);
    rotZ[1][0] = -sin(rz);
    rotZ[1][1] = cos(rz);
    
    mat4 scale = mat4(1.0f);
    scale[0][0] = sx;
    scale[1][1] = sy;
    scale[2][2] = sz;
    
    mat4 rot = rotZ * rotY * rotX;
    modelMatrix = trans * rot * scale;
    
    update();
}

void GLWidget::updateViewMatrix() {
    // Part 2 - Construct a view matrix and upload as a uniform variable
    // to the cube and grid programs. Update your vertex shader accordingly.
    vec3 eye(20*cos(camAngle), camY, 20*sin(camAngle));
    vec3 at(0, 0, 0);
    vec3 up(0, 1, 0);
    
    vec3 z = normalize(eye - at);
    vec3 x = normalize(cross(up, z));
    vec3 y = cross(z, x);
    
    vec4 c1(x, 0);
    vec4 c2(y, 0);
    vec4 c3(z, 0);
    vec4 c4(eye, 1);
    viewMatrix = inverse(mat4(c1, c2, c3, c4));
    
    update();
}

void GLWidget::updateProjMatrix(int w, int h){
	float ar = (float)w/h;
    float fov = cameraFov * M_PI/180;
	float tanHalf = tan(fov/2);
	float nearZ = 0.5;
	float farZ = 50;
    
    projMatrix[0][0] = 1/(ar*tanHalf);
    projMatrix[1][1] = 1/tanHalf;
    projMatrix[2][2] = (nearZ + farZ)/(nearZ - farZ);
    projMatrix[2][3] = -1;
    projMatrix[3][2] = (2 * farZ * nearZ)/(nearZ - farZ);
    projMatrix[3][3] = 0;
    
    glUseProgram(cubeProg);
    glUniformMatrix4fv(cubeProjMatrixLoc, 1, false, value_ptr(projMatrix));

    glUseProgram(gridProg);
    glUniformMatrix4fv(gridProjMatrixLoc, 1, false, value_ptr(projMatrix));
    
    update();
}
