#include <QImage>
#include <QColor>
#include <iostream>
#include <string>

using namespace std;

class Point{
public:
	double x, y;
	double r, g, b;

	Point() : x(0), y(0), r(0), g(0), b(0){
		
	}
};

istream& operator>>(istream& stream, Point& p){
	char ch;
	cin >> p.x >> ch >> p.y >> ch >> p.r >> ch >> p.g >> ch >> p.b;
	return stream;
}

ostream& operator<<(ostream& stream, const Point& p){
	stream << p.x << ", " << p.y << " : " << p.r << ", " << p.g << ", " << p.b;
	return stream;
}

int main(int argc, char** argv) {
    /* 
      Prompt user for 3 points separated by whitespace.

      Part 1:
          You'll need to get the x and y coordinate as floating point values
          from the user for 3 points that make up a triangle.

      Part 3:
          You'll need to also request 3 colors from the user each having
          3 floating point values (red, green and blue) that range from 0 to 1.
    */

	cout << "Enter 3 points as x,y:r,g,b" << endl << "> ";
	Point pt1;
	cin >> pt1;

	cout << "> ";
	Point pt2;
	cin >> pt2;

	cout << "> ";
	Point pt3;
	cin >> pt3;

	cout << "You entered: " << endl;
	cout << pt1 << endl;
	cout << pt2 << endl;
	cout << pt3 << endl;

    // create an image 640 pixels wide by 480 pixels tall
    // with a standard 32 bit red, green, blue format
    QImage image(640, 480, QImage::Format_RGB32); 

    /* 
      Part 1:
          Calculate the bounding box of the 3 provided points and loop
          over each pixel in that box and set it to white using:

          image.setPixel(x,y, qRgb(255,255,255));

      Part 2:
          Modify your loop from part 1. Using barycentric coordinates, 
          determine if the pixel lies within the triangle defined by 
          the 3 provided points. If it is color it white, otherwise
          move on to the next pixel.

          For more on barycentric coordinates:
          http://en.wikipedia.org/wiki/Barycentric_coordinate_system

      Part 3:
          For each pixel in the triangle, calculate the color based on
          the calculated barycentric coordinates and the 3 provided
          colors. Your colors should have been entered as floating point
          numbers from 0 to 1. Since we have a 32 bit image, the red, 
          green and blue components range from 0 to 255. Be sure to make
          the conversion.
    */

	double minx, maxx, miny, maxy;
	minx = min(pt1.x, min(pt2.x, pt3.x));
	maxx = max(pt1.x, max(pt2.x, pt3.x));
	miny = min(pt1.y, min(pt2.y, pt3.y));
	maxy = max(pt1.y, max(pt2.y, pt3.y));

	for(int xi = minx; xi < maxx; xi++){
		for(int yi = miny; yi < maxy; yi++){
			double gamma1 = ((pt2.y - pt3.y)*(xi - pt3.x) + (pt3.x - pt2.x)*(yi - pt3.y)) / 
					((pt2.y - pt3.y)*(pt1.x - pt3.x) + (pt3.x - pt2.x)*(pt1.y - pt3.y));

			double gamma2 = ((pt3.y - pt1.y)*(xi - pt3.x) + (pt1.x - pt3.x)*(yi - pt3.y)) /
					((pt2.y - pt3.y)*(pt1.x - pt3.x) + (pt3.x - pt2.x)*(pt1.y - pt3.y));

			double gamma3 = 1 - gamma1 - gamma2;

			if(gamma1 > 0 && gamma2 > 0 && gamma3 > 0){
				double r,g,b;
				r = gamma1*pt1.r + gamma2*pt2.r + gamma3*pt3.r;
				b = gamma1*pt1.b + gamma2*pt2.b + gamma3*pt3.b;
				g = gamma1*pt1.g + gamma2*pt2.g + gamma3*pt3.g;
				image.setPixel(xi, yi, qRgb(255*r, 255*g, 255*b));
			}
		}
	}

    if(image.save("triangle.jpg",0,100)) {
        cout << "Output triangle.jpg" << endl;
    } else {
        cout << "Unable to save triangle.jpg" << endl;
    }
}
